# buildspace Solana GIF Portal Project

### **Welcome 👋**
To get started with this course, clone this repo and follow these commands:

1. Run `npm install` at the root of your directory
2. Run `npm run start` to start the project
3. Start coding!

### **What is the .vscode Folder?**
If you use VSCode to build your app, we included a list of suggested extensions that will help you build this project! Once you open this project in VSCode, you will see a popup asking if you want to download the recommended extensions :).



### **Questions?**
Have some questions make sure you head over to your [buildspace Dashboard](https://app.buildspace.so/courses/CObd6d35ce-3394-4bd8-977e-cbee82ae07a3) and link your Discord account so you can get access to helpful channels and your instructor!


### setting up solana set up running a local validator ###

export PATH="/home/demon/.local/share/solana/install/active_release/bin:$PATH"

solana config set --url localhost
solana config get
solana-test-validator
npm install -g mocha
cargo install --git https://github.com/project-serum/anchor anchor-cli --locked

npm install @project-serum/anchor @solana/web3.js


<!-- inits a sample solana project -->
anchor init myepicproject --javascript
<!-- Create a local keypair is basically a solana wallet can geneterate addresses like below -->
solana-keygen new

Wrote new keypair to /home/demon/.config/solana/id.json
===============================================================================
pubkey: ATZMBgsseAsm65QgsacF5TYc2oa31ZZTiKQ9shVGJtca
===============================================================================
Save this seed phrase and your BIP39 passphrase to recover your new keypair:
aspect robust female outdoor bread pencil theme chest isolate veteran post tiny
===============================================================================
solana address
ATZMBgsseAsm65QgsacF5TYc2oa31ZZTiKQ9shVGJtca

<!-- runs the anchor project (test is the name im pretty sure) -->
anchor test

<!-- switch to devnet: -->
solana config set --url devnet
<!-- airdrop sol in testnet -->
solana airdrop 5

<!-- in anchor toml -->
[programs.devnet]
cluster = "devnet"


anchor build

solana address -k target/deploy/myepicproject-keypair.json

<!-- So, we need to change this program id in declare_id! in lib.rs to the one output by solana address -k target/deploy/myepicproject-keypair.json -->

<!-- Now, go to Anchor.toml and under [programs.devnet] you'll see something like myepicproject = "Fg6PaFpoGXkYsidMpWTK6W2BeZ7FEfcYkg476zPFsLnS". Go ahead and change this id to the same id output when you run  -->

<!-- actually deploy the app once youve linked up the account ids in lib.rs and anchor.toml -->
anchor deploy

to connect your react app with the anchor rust app copy the json from
target/idl/mysolanaapp.json in the rust app, and create a json file of the same name in your react app in the src directory

<!-- fund phantom wallet -->
after connecting your phantom wallet to dev net fund it 
solana airdrop 5 INSERT_YOUR_PHANTOM_PUBLIC_ADDRESS_HERE  --url https://api.devnet.solana.com


<!-- const fs = require('fs')
const anchor = require("@project-serum/anchor")

const account = anchor.web3.Keypair.generate()

fs.writeFileSync('./keypair.json', JSON.stringify(account)) -->

<!-- All this script does is it will write a key pair directly to our file system, that way anytime people come to our web app they'll all load the same key pair.
When you're ready to run this go ahead and do: -->

cd src (in react app)
node createKeyPair.js


mkdir ~/my-solana-wallet
solana-keygen new --outfile ~/my-solana-wallet/my-keypair.json

solana-keygen pubkey ~/my-solana-wallet/my-keypair.json

verify your generated wallet key pair
solana-keygen verify <PUBKEY> ~/my-solana-wallet/my-keypair.json

solana-keygen verify 85U6HiBUA1WyZ3jcUxECN2wHRBSTbqpzLdmeqSd9miwK /home/demon/solana-wallet/my-keypair.json
Verification for public key: 85U6HiBUA1WyZ3jcUxECN2wHRBSTbqpzLdmeqSd9miwK: Success

<!-- You can create as many wallet addresses as you like. Simply re-run the steps in Generate a File System Wallet and make sure to use a new filename or path with the --outfile argument. Multiple wallet addresses can be useful if you want to transfer tokens between your own accounts for different purposes. -->

<!-- this checks what nodes you can connect to -->
solana config get

Config File: /home/demon/.config/solana/cli/config.yml
RPC URL: https://api.devnet.solana.com 
WebSocket URL: wss://api.devnet.solana.com/ (computed)
Keypair Path: /home/demon/.config/solana/id.json 
Commitment: confirmed 

<!-- now we can connect to dev net through a node -->
solana config set --url https://api.devnet.solana.com

<!-- Ensure Versions Match# -->

<!-- Though not strictly necessary, the CLI will generally work best when its version matches the software version running on the cluster. To get the locally-installed CLI version, run: -->
solana --version

<!-- To get the cluster version, run: -->
solana cluster-version

<!-- create testnet coins -->

solana airdrop 1 <RECIPIENT_ACCOUNT_ADDRESS> --url https://api.devnet.solana.com

<!-- make a transfer -->
solana transfer --from <KEYPAIR> <RECIPIENT_ACCOUNT_ADDRESS> 0.5 --allow-unfunded-recipient --url https://api.devnet.solana.com --fee-payer <KEYPAIR>



$ solana-keygen new --outfile my_solana_wallet.json   # Creating my first wallet, a file system wallet
Generating a new keypair
For added security, enter a passphrase (empty for no passphrase):
Wrote new keypair to my_solana_wallet.json
==========================================================================
pubkey: DYw8jCTfwHNRJhhmFcbXvVDTqWMEVFBX6ZKUmG5CNSKK                          # Here is the address of the first wallet
==========================================================================
Save this seed phrase to recover your new keypair:
width enhance concert vacant ketchup eternal spy craft spy guard tag punch    # If this was a real wallet, never share these words on the internet like this!
==========================================================================

$ solana airdrop 1 DYw8jCTfwHNRJhhmFcbXvVDTqWMEVFBX6ZKUmG5CNSKK --url https://api.devnet.solana.com  # Airdropping 1 SOL to my wallet's address/pubkey
Requesting airdrop of 1 SOL from 35.233.193.70:9900
1 SOL

$ solana balance DYw8jCTfwHNRJhhmFcbXvVDTqWMEVFBX6ZKUmG5CNSKK --url https://api.devnet.solana.com # Check the address's balance
1 SOL

$ solana-keygen new --no-outfile  # Creating a second wallet, a paper wallet
Generating a new keypair
For added security, enter a passphrase (empty for no passphrase):
====================================================================
pubkey: 7S3P4HxJpyyigGzodYwHtCxZyUQe9JiBMHyRWXArAaKv                   # Here is the address of the second, paper, wallet.
====================================================================
Save this seed phrase to recover your new keypair:
clump panic cousin hurt coast charge engage fall eager urge win love   # If this was a real wallet, never share these words on the internet like this!
====================================================================

$ solana transfer --from my_solana_wallet.json 7S3P4HxJpyyigGzodYwHtCxZyUQe9JiBMHyRWXArAaKv 0.5 --allow-unfunded-recipient --url https://api.devnet.solana.com --fee-payer my_solana_wallet.json  # Transferring tokens to the public address of the paper wallet
3gmXvykAd1nCQQ7MjosaHLf69Xyaqyq1qw2eu1mgPyYXd5G4v1rihhg1CiRw35b9fHzcftGKKEu4mbUeXY2pEX2z  # This is the transaction signature

$ solana balance DYw8jCTfwHNRJhhmFcbXvVDTqWMEVFBX6ZKUmG5CNSKK --url https://api.devnet.solana.com
0.499995 SOL  # The sending account has slightly less than 0.5 SOL remaining due to the 0.000005 SOL transaction fee payment

$ solana balance 7S3P4HxJpyyigGzodYwHtCxZyUQe9JiBMHyRWXArAaKv --url https://api.devnet.solana.com
0.5 SOL  # The second wallet has now received the 0.5 SOL transfer from the first wallet

====================================================================

Receive Tokens#

To receive tokens, you will need an address for others to send tokens to. In Solana, the wallet address is the public key of a keypair. There are a variety of techniques for generating keypairs. The method you choose will depend on how you choose to store keypairs. Keypairs are stored in wallets. Before receiving tokens, you will need to create a wallet. Once completed, you should have a public key for each keypair you generated. The public key is a long string of base58 characters. Its length varies from 32 to 44 characters.
Send Tokens#

If you already hold SOL and want to send tokens to someone, you will need a path to your keypair, their base58-encoded public key, and a number of tokens to transfer. Once you have that collected, you can transfer tokens with the solana transfer command:
solana transfer --from <KEYPAIR> <RECIPIENT_ACCOUNT_ADDRESS> <AMOUNT> --fee-payer <KEYPAIR>
